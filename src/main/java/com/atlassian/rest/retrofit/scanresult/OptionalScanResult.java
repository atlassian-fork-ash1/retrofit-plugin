package com.atlassian.rest.retrofit.scanresult;

public abstract class OptionalScanResult
{
    protected boolean isSkipped = false;

    public boolean isSkipped()
    {
        return isSkipped;
    }
}
