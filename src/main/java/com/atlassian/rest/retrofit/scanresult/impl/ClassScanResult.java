package com.atlassian.rest.retrofit.scanresult.impl;

import com.atlassian.rest.retrofit.scanresult.OptionalScanResult;

import java.util.List;

public class ClassScanResult extends OptionalScanResult
{
    private String packageName;
    private String className;
    private String path;
    private List<String> consumes;
    private List<String> produces;
    private List<MethodScanResult> methods;

    private ClassScanResult() {}

    public static ClassScanResult skip()
    {
        ClassScanResult skippedInstance = new ClassScanResult();
        skippedInstance.isSkipped = true;
        return skippedInstance;
    }

    public String getPackageName()
    {
        return packageName;
    }

    public String getClassName()
    {
        return className;
    }

    public String getPath()
    {
        return path;
    }

    public List<String> getConsumes()
    {
        return consumes;
    }

    public List<String> getProduces()
    {
        return produces;
    }

    public List<MethodScanResult> getMethods()
    {
        return methods;
    }

    public static class Builder
    {
        private String packageName;
        private String className;
        private String path;
        private List<String> consumes;
        private List<String> produces;
        private List<MethodScanResult> methods;

        public Builder packageName(String packageName)
        {
            this.packageName = packageName;
            return this;
        }

        public Builder className(String className)
        {
            this.className = className;
            return this;
        }

        public Builder path(String path)
        {
            this.path = path;
            return this;
        }

        public Builder consumes(List<String> consumes)
        {
            this.consumes = consumes;
            return this;
        }

        public Builder produces(List<String> produces)
        {
            this.produces = produces;
            return this;
        }

        public Builder methods(List<MethodScanResult> methods)
        {
            this.methods = methods;
            return this;
        }

        public ClassScanResult build()
        {
            ClassScanResult instance = new ClassScanResult();
            instance.className = this.className;
            instance.consumes = this.consumes;
            instance.methods = this.methods;
            instance.packageName = this.packageName;
            instance.path = this.path;
            instance.produces = produces;

            return instance;
        }
    }
}
