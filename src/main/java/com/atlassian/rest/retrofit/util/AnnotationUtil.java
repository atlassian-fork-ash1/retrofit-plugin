package com.atlassian.rest.retrofit.util;

import com.google.common.collect.Lists;
import javassist.bytecode.annotation.Annotation;
import javassist.bytecode.annotation.ArrayMemberValue;
import javassist.bytecode.annotation.MemberValue;
import javassist.bytecode.annotation.StringMemberValue;

import java.util.List;

public class AnnotationUtil
{
    public static String getAnnotationValue(Annotation an)
    {
        if (an == null) return null;
        return ((StringMemberValue) an.getMemberValue("value")).getValue();
    }

    public static List<String> getAnnotationValues(Annotation an)
    {
        List<String> annotationValues = Lists.newArrayList();
        if (an == null) return null;

        MemberValue[] values = ((ArrayMemberValue) an.getMemberValue("value")).getValue();
        for (MemberValue value : values)
        {
            annotationValues.add(((StringMemberValue) value).getValue());
        }
        return annotationValues;
    }
}
