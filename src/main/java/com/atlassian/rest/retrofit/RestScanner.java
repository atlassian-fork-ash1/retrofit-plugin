package com.atlassian.rest.retrofit;

import com.atlassian.rest.retrofit.generator.GeneratorContext;
import com.atlassian.rest.retrofit.generator.impl.RestClientInterfaceGenerator;
import com.atlassian.rest.retrofit.scanner.impl.ClassScanner;
import com.atlassian.rest.retrofit.scanner.ScannerContext;
import com.atlassian.rest.retrofit.scanresult.impl.ClassScanResult;
import javassist.NotFoundException;
import javassist.bytecode.*;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.plugin.logging.Log;
import org.reflections.Reflections;
import org.reflections.adapters.JavassistAdapter;
import org.reflections.scanners.AbstractScanner;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;

public class RestScanner extends AbstractScanner
{
    private final RestScannerConfiguration configuration;
    private final Log logger;
    private final File generatedSourceCodeFolder;

    public RestScanner(RestScannerConfiguration configuration)
    {
        this.configuration = configuration;
        this.logger = configuration.getLogger();
        this.generatedSourceCodeFolder = new File(configuration.getGeneratedSourceCodeFolder());

        try
        {
            rock();
        }
        catch (NotFoundException e)
        {
            e.printStackTrace();
        }
    }

    public void rock() throws NotFoundException
    {
        // Time to delegate to father
        ConfigurationBuilder config = new ConfigurationBuilder();

        config.setUrls(configuration.getUrls())
                .filterInputsBy(new FilterBuilder().includePackage(configuration.getScanningPackage()))
                .setScanners(this);

        createGeneratedSourceCodeFolder();

        ClassLoader originalClassLoader = Thread.currentThread().getContextClassLoader();
        logger.info("Stored the original class loader");

        switchToNewClassLoader(originalClassLoader, configuration);

        // It will be all rocked here
        Reflections reflections = new Reflections(config.build());

        Thread.currentThread().setContextClassLoader(originalClassLoader);
        logger.info("Restored the original class loader.");
    }

    private void createGeneratedSourceCodeFolder()
    {
        if (!generatedSourceCodeFolder.exists() || !generatedSourceCodeFolder.isDirectory())
        {
            if (generatedSourceCodeFolder.mkdir())
            {
                logger.info(generatedSourceCodeFolder.getAbsolutePath() + " where the source code files is generated is created successfully");
            }
            else
            {
                throw new IllegalArgumentException(generatedSourceCodeFolder.getAbsolutePath() + " couldn't be created." +
                        " Retrofit plugin stops immediately because source code files won't be able to generated.");
            }
        }
    }

    private void switchToNewClassLoader(ClassLoader originalClassLoader, RestScannerConfiguration configuration)
    {
        Collection<URL> urls = new ArrayList<URL>();

        for (Artifact artifact : configuration.getDependencyArtifacts())
        {
            try
            {
                urls.add(artifact.getFile().toURI().toURL());
            }
            catch (MalformedURLException e)
            {
                logger.error(e);
            }
        }

        urls.addAll(configuration.getUrls());

        logger.info("Prepared the dependency artifacts and output class folder.");
        logger.debug("Class path urls = \n" + urls.toString().replace(",","\n")) ;

        Thread.currentThread().setContextClassLoader(new URLClassLoader(urls.toArray(new URL[urls.size()]), originalClassLoader));
        logger.info("Switched to new class loader cloned from the original class loader" +
                " with dependency artifacts and output folder classes added.");
    }

    @Override
    public void scan(final Object cls)
    {
        ClassFile classFile = (ClassFile) cls;
        try
        {
            scanClass(classFile);
        }
        catch (Exception e)
        {
            logger.error("Unable to run byte code scanner on class " + classFile.getName() + ". Continuing to the next class...", e);
        }
    }

    private void scanClass(final ClassFile classFile) throws Exception
    {
        ScannerContext scannerContext = new ScannerContext(logger, (JavassistAdapter) getMetadataAdapter());
        ClassScanResult classScanResult = ClassScanner.get().scan(classFile, scannerContext);

        if (!classScanResult.isSkipped())
        {
            GeneratorContext generatorContext = new GeneratorContext(logger, generatedSourceCodeFolder);
            RestClientInterfaceGenerator.get().generateRestClient(classScanResult, generatorContext);
        }
    }
}