package com.atlassian.rest.retrofit.scanner.impl;

import com.atlassian.rest.retrofit.scanner.ScannerContext;
import com.atlassian.rest.retrofit.scanresult.impl.MethodScanResult;
import com.atlassian.rest.retrofit.scanresult.impl.ParamScanResult;
import com.atlassian.rest.retrofit.util.AnnotationUtil;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.MethodInfo;
import org.apache.maven.plugin.logging.Log;
import org.reflections.adapters.JavassistAdapter;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;
import java.util.Set;

public class MethodScanner
{
    private static final Set<String> METHOD_ANNOTATION_TYPES = Sets.newHashSet(
            GET.class.getCanonicalName(),
            POST.class.getCanonicalName(),
            PUT.class.getCanonicalName(),
            DELETE.class.getCanonicalName()
    );

    private static final MethodScanner methodScanner = new MethodScanner();

    public static MethodScanner get()
    {
        return methodScanner;
    }

    private MethodScanner() {}

    public List<MethodScanResult> scan(List<MethodInfo> methods, ScannerContext scannerContext)
    {
        List<MethodScanResult> methodScanResultList = Lists.newArrayList();
        for (MethodInfo method : methods)
        {
            MethodScanResult methodScanResult = scanMethod(method, scannerContext);
            if (!methodScanResult.isSkipped())
            {
                methodScanResultList.add(methodScanResult);
            }
        }

        return methodScanResultList;
    }

    private MethodScanResult scanMethod(final MethodInfo methodInfo, ScannerContext scannerContext)
    {
        JavassistAdapter adapter = scannerContext.getAdapter();
        Log logger = scannerContext.getLogger();

        Set<String> methodAnnotations = Sets.newHashSet(adapter.getMethodAnnotationNames(methodInfo));
        Set<String> methodDetectionResult = Sets.intersection(METHOD_ANNOTATION_TYPES, methodAnnotations);
        if (!methodDetectionResult.isEmpty())
        {
            logger.debug("++++ Detected method " + methodInfo + " ...");

            String methodType = Iterables.getFirst(methodDetectionResult, GET.class.getCanonicalName());

            AnnotationsAttribute attrs = (AnnotationsAttribute) methodInfo.getAttribute(AnnotationsAttribute.visibleTag);
            String path = AnnotationUtil.getAnnotationValue(attrs.getAnnotation(Path.class.getCanonicalName()));

            List<String> produces = AnnotationUtil.getAnnotationValues(attrs.getAnnotation(Produces.class.getCanonicalName()));

            List<ParamScanResult> paramScanResultList = ParamScanner.get().scan(methodInfo, scannerContext);

            String methodName = methodInfo.getName();
            return new MethodScanResult.Builder()
                    .path(path)
                    .type(methodType)
                    .name(methodName)
                    .produces(produces)
                    .params(paramScanResultList)
                    .build();
        }
        else
        {
            logger.debug("---- Skipped method " + methodInfo + " ...");
            return MethodScanResult.skip();
        }
    }
}
