package com.atlassian.rest.retrofit.scanner.impl;

import com.atlassian.rest.retrofit.scanresult.impl.PayloadScanResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.module.jsonSchema.JsonSchema;
import com.fasterxml.jackson.module.jsonSchema.JsonSchemaGenerator;
import com.fasterxml.jackson.module.scala.DefaultScalaModule$;
import org.apache.maven.plugin.logging.Log;

public class PayloadScanner
{
    private static ObjectMapper mapper;
    private static JsonSchemaGenerator generator;
    private static ObjectWriter prettyPrinter;

    private PayloadScanner()
    {
        mapper = new ObjectMapper();
        mapper.registerModule(DefaultScalaModule$.MODULE$);

        generator = new JsonSchemaGenerator(mapper);
        prettyPrinter = mapper.writerWithDefaultPrettyPrinter();
    }

    public static PayloadScanner get()
    {
        return new PayloadScanner();
    }

    public PayloadScanResult scan(String payloadDataType, Log logger) throws ClassNotFoundException, JsonProcessingException
    {
        Class clazz = Thread.currentThread().getContextClassLoader().loadClass(payloadDataType);
        JsonSchema jsonSchema = generator.generateSchema(clazz);

        logger.debug("Payload JSON schema: \n" + prettyPrinter.writeValueAsString(jsonSchema));

        String schema = mapper.writeValueAsString(jsonSchema);

        return new PayloadScanResult.Builder()
                                    .dataType(payloadDataType)
                                    .schema(schema)
                                    .build();
    }
}
