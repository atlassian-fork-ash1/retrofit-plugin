package com.atlassian.rest.retrofit.scanner.impl;

import com.atlassian.rest.retrofit.scanner.ScannerContext;
import com.atlassian.rest.retrofit.scanresult.impl.ParamScanResult;
import com.atlassian.rest.retrofit.scanresult.impl.PayloadScanResult;
import com.atlassian.rest.retrofit.util.AnnotationUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import javassist.bytecode.MethodInfo;
import javassist.bytecode.ParameterAnnotationsAttribute;
import javassist.bytecode.annotation.Annotation;
import org.apache.commons.lang3.ClassUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.logging.Log;
import org.reflections.adapters.JavassistAdapter;

import javax.ws.rs.FormParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import java.util.List;
import java.util.Set;

public class ParamScanner
{
    private static final Set<String> ACCEPT_PARAM_ANNOTATION_TYPES = Sets.newHashSet(
            QueryParam.class.getCanonicalName(),
            PathParam.class.getCanonicalName(),
            FormParam.class.getCanonicalName()
    );

    private static final Set<String> IGNORE_PARAM_ANNOTATION_TYPES = Sets.newHashSet(
            Context.class.getCanonicalName()
    );

    private static final ParamScanner paramScanner = new ParamScanner();

    public static ParamScanner get()
    {
        return paramScanner;
    }

    private ParamScanner() {}

    public List<ParamScanResult> scan(MethodInfo methodInfo, ScannerContext scannerContext)
    {
        JavassistAdapter adapter = scannerContext.getAdapter();
        Log logger = scannerContext.getLogger();

        List<String> paramDataTypes = adapter.getParameterNames(methodInfo);

        // Get the array of param's annotations. This array is nullable.
        ParameterAnnotationsAttribute paramAnnotationsAttr = (ParameterAnnotationsAttribute) methodInfo.getAttribute(ParameterAnnotationsAttribute.visibleTag);
        Annotation[][] paramAnnotationsArray;
        if (paramAnnotationsAttr == null)
        {
            paramAnnotationsArray = new Annotation[0][0];
        }
        else
        {
            paramAnnotationsArray = paramAnnotationsAttr.getAnnotations();
        }

        return scanParams(paramAnnotationsArray, paramDataTypes, logger);
    }

    private List<ParamScanResult> scanParams(Annotation[][] paramAnnotationsArray, List<String> paramDataTypes, Log logger)
    {
        List<ParamScanResult> paramScanResultList = Lists.newArrayList();
        boolean skipPayload = false;

        for (int paramIndex = 0; paramIndex < paramDataTypes.size(); paramIndex++)
        {
            String paramDataType = paramDataTypes.get(paramIndex);

            Annotation[] paramAnnotations;
            if (paramAnnotationsArray.length <= 0)
            {
                // This is to prevent IndexOutOfBoundsException when method does not take any arguments
                paramAnnotations = new Annotation[0];
            }
            else
            {
                paramAnnotations = paramAnnotationsArray[paramIndex];
            }

            ParamScanResult paramScanResult = scanParam(paramAnnotations, paramDataType, logger, skipPayload);

            if (paramScanResult.isPayload())
            {
                // Only 1 payload object is allowed
                skipPayload = true;
            }

            if (!paramScanResult.isSkipped())
            {
                paramScanResultList.add(paramScanResult);
            }
        }

        return paramScanResultList;
    }

    /**
     *
     * Magic here
     * 1. Construct result for @PathParam and @QueryParam and @FormParam, param name will be the same as value inside the annotation
     * 2. Will ignore param with @Context
     * 3. All non-annotated param will be marked as payload (and there's maximum only one payload per method call), actually depends on
     * the data type
     */
    private ParamScanResult scanParam(final Annotation[] paramAnnotations, String paramDataType, Log logger, boolean skipPayload)
    {
        ParamScanResult.Builder paramScanResultBuilder = new ParamScanResult.Builder();
        String paramName;

        if (paramAnnotations.length <= 0)
        {
            if (!skipPayload)
            {
                paramName = StringUtils.uncapitalize(ClassUtils.getShortClassName(paramDataType));
                logger.debug("++++++++ Detected payload " + paramDataType + " ...");

                PayloadScanResult payloadScanResult;
                try
                {
                    payloadScanResult = PayloadScanner.get().scan(paramDataType, logger);
                }
                catch (ClassNotFoundException e)
                {
                    logger.error("-------- Skipped payload " + paramDataType + " ...", e);
                    return ParamScanResult.skip();
                }
                catch (JsonProcessingException e)
                {
                    logger.error("-------- Skipped payload " + paramDataType + " ...", e);
                    return ParamScanResult.skip();
                }

                paramScanResultBuilder.payload(payloadScanResult);
            }
            else
            {
                logger.debug("++++++++ Skipped payload " + paramDataType + " because another payload is detected already ...");
                return ParamScanResult.skip();
            }
        }
        else if (paramAnnotations.length > 1)
        {
            logger.error("-------- Skipped param " + paramDataType + " because it has more than 1 annotation which is not supported yet ...");
            return ParamScanResult.skip();
        }
        else
        {
            Annotation paramAnnotation = paramAnnotations[0];
            String paramAnnotationType = paramAnnotation.getTypeName();

            if (ACCEPT_PARAM_ANNOTATION_TYPES.contains(paramAnnotationType))
            {
                paramScanResultBuilder.type(paramAnnotationType);
                paramName = AnnotationUtil.getAnnotationValue(paramAnnotation);
                logger.debug("++++++++ Detected " + ClassUtils.getShortClassName(paramAnnotationType) + " " + paramName + " (L" + paramDataType + ") ...");
            }
            else if (IGNORE_PARAM_ANNOTATION_TYPES.contains(paramAnnotationType))
            {
                logger.debug("-------- Skipped param with ignored annotation" + paramAnnotationType + " ...");
                return ParamScanResult.skip();
            }
            else
            {
                logger.debug("-------- Skipped param with unsupported annotation " + paramAnnotationType + " ...");
                return ParamScanResult.skip();
            }
        }

        paramScanResultBuilder.name(paramName);
        paramScanResultBuilder.dataType(paramDataType);

        return paramScanResultBuilder.build();
    }
}
