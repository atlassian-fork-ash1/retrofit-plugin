package com.atlassian.rest.retrofit.scanner;

import org.apache.maven.plugin.logging.Log;
import org.reflections.adapters.JavassistAdapter;

public class ScannerContext
{
    private final JavassistAdapter adapter;
    private final Log logger;

    public ScannerContext(Log logger, JavassistAdapter adapter)
    {
        this.logger = logger;
        this.adapter = adapter;
    }

    public Log getLogger()
    {
        return logger;
    }

    public JavassistAdapter getAdapter()
    {
        return adapter;
    }
}
